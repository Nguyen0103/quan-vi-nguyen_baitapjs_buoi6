// bài 1
document.querySelector('.xem-ket-qua').addEventListener('click',function(){
    sum = 0;
    i1 = 0;
    while (sum < 10000) {
        i1++;
        sum += i1;
    }
    console.log({sum, i1});
    document.querySelector('.in-ket-qua').innerHTML = `<div>Số nguyên dương nhỏ nhất: ${i1}</div>`
});

// bài 2
document.querySelector('.tinh-tong-bai-2').addEventListener('click',function(){
    var NumberX = document.querySelector('#txt-nhap-so-x').value * 1;//X
    var NumberN2 = document.querySelector('#txt-nhap-so-N').value * 1;//N
    var T = 1; //tính tích
    var S = 0; //tính tổng phần tử
    for (var i2 = 1; i2 <= NumberN2 ; i2++) {
        T = T * NumberX;
        S = S + T;
    }
    console.log('Tổng',S)
    document.querySelector('.in-ket-qua-bai-2').innerHTML = `<div>Tổng: ${S}</div>`
});

// bài 3
document.querySelector('.TinhGiaiThua').addEventListener('click',function(){
    var NummbeN3 = document.querySelector('#txt-giai-thua').value * 1;
    var GiaiThua = 1;

    for (var i3 = 1; i3 <= NummbeN3; i3++ ) {
         GiaiThua *= i3;
    }

    console.log(GiaiThua)
    document.querySelector('.in-giai-thua').innerHTML = `<div>Tổng giai thừa: ${GiaiThua}</div>`

});

// bài 4
document.querySelector('.Tao-div').addEventListener('click',function(){
    var Even = `<div class="bg-danger">Div shẵn</div>`
    var Odd = `<div class="bg-primary">Div lẻ</div>`
    var content = ""

    var getBg = function(i) {
        if(i % 2 == 0){
            return Even
        }
        else{
            return Odd
        }
    }
    
    for(var i = 1; i <= 10; i++){
        content = content + getBg(i)
    }
    document.querySelector('.in-ket-qua-4').innerHTML = `<div> ${content} </div>`
});
